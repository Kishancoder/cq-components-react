# Banners
 
![picture](https://bitbucket.org/coqtail/cq-ui-elements/src/master/src/UI/Banners/preview/banners-preview.png)
 
## Database
Component use cq-slider with linking to taxons and products
 
Slide fields :

* Title
* Image
* Link

## Props
* name: name of slider on backend
* numberOfItemToDisplay:DEFAULT: 2: number of items to display