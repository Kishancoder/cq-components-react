'use strict';
Object.defineProperty(exports, '__esModule', {
    value: true
});
var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ('value' in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor)
        }
    }
    return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor
    }
}();
var _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];
        for (var key in source) {
            if (Object.prototype.hasOwnProperty.call(source, key)) {
                target[key] = source[key]
            }
        }
    }
    return target
};
var _react = require('react');
var _react2 = _interopRequireDefault(_react);
var _reactRedux = require('react-redux');
var _cqBaseReact = require('cq-base-react');
var _reactRouterDom = require('react-router-dom');
var _classnames = require('classnames');
var _classnames2 = _interopRequireDefault(_classnames);
var _lodash = require('lodash');
var _lodash2 = _interopRequireDefault(_lodash);
var _Image = require('../../UI/Image');
var _Image2 = _interopRequireDefault(_Image);
var _Button = require('../../UI/Button');
var _Button2 = _interopRequireDefault(_Button);
var _Translations = require('../../Common/Translations');
var _Translations2 = _interopRequireDefault(_Translations);
var _iconHeart = require('./images/icon--heart.svg');
var _iconHeart2 = _interopRequireDefault(_iconHeart);
var _WishlistModal = require('../WishlistModal');
var _withSettings = require('../../HOC/withSettings');
var _withSettings2 = _interopRequireDefault(_withSettings);
var _withOverride = require('../../HOC/withOverride');
var _withOverride2 = _interopRequireDefault(_withOverride);
require('./css/styles.css');
var _QuickAddProduct = require('../QuickAddProduct');
var _QuickAddProduct2 = _interopRequireDefault(_QuickAddProduct);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    }
}

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError('Cannot call a class as a function')
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError('this hasn\'t been initialised - super() hasn\'t been called')
    }
    return call && (typeof call === 'object' || typeof call === 'function') ? call : self
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== 'function' && superClass !== null) {
        throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass)
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
        constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass
}
var openModal = function openModal(props) {
    var openModal = props.openModal,
        product = props.product;
    var component = _react2.default.createElement(_WishlistModal.WishlistModalWrapper, {
        leftPosition: _react2.default.createElement(ProductItem, _extends({
            product: product
        }, props)),
        centerPosition: _react2.default.createElement(_WishlistModal.WishlistModal, {
            product: product
        })
    });
    openModal(component)
};
var renderWishlistHeart = function renderWishlistHeart(props) {
    var hasWishlist = props.hasWishlist;
    if (!hasWishlist) {
        return null
    }
    return _react2.default.createElement('div', {
        className: 'wishlist__icon-container',
        onClick: function onClick() {
            return openModal(props)
        }
    }, _react2.default.createElement('img', {
        src: _iconHeart2.default,
        className: 'wishlist__icon',
        alt: ''
    }))
};
var renderSizes = function renderSizes(product) {
    return _react2.default.createElement('div', {
        className: 'product__sizes'
    }, (product.getOptionValueByOptionTypeName('size') || []).map(function (size) {
        var classes = (0, _classnames2.default)('product__size', {
            'product__size--out-of-stock': !size.getProperty('in_stock') && !size.getProperty('is_backorderable')
        });
        return _react2.default.createElement('span', {
            className: classes,
            key: size.getId()
        }, size.getPresentation())
    }))
};
var ProductItem = function (_React$Component) {
    _inherits(ProductItem, _React$Component);

    function ProductItem(props) {
        _classCallCheck(this, ProductItem);
        var _this = _possibleConstructorReturn(this, (ProductItem.__proto__ || Object.getPrototypeOf(ProductItem)).call(this, props));
        _initialiseProps.call(_this);
        var product = props.product,
            imageSize = props.imageSize,
            imageCdnOptions = props.imageCdnOptions,
            hasHoverImage = props.hasHoverImage;
        var image = product.getProperty('master') ? product.getImageBySize(imageSize, {
            imageCdnOptions: imageCdnOptions
        }) : _cqBaseReact.libs.getImageByUrl(product.getProperty('images[0].' + imageSize, {
            imageCdnOptions: imageCdnOptions
        }));
        var hoverImage = void 0;
        if (hasHoverImage) {
            var hoverImageSrc = product.getProperty('master.images[1].' + imageSize, {
                imageCdnOptions: imageCdnOptions
            });
            hoverImage = product.getProperty('master') ? _cqBaseReact.libs.getImageByUrl(hoverImageSrc) : null;
            if (!hoverImageSrc) {
                hoverImage = null
            }
        }
        _this.state = {
            quickAddOptionsVisible: false,
            initialImage: image,
            activeImage: image,
            hoverImage: hoverImage
        };
        return _this
    }
    _createClass(ProductItem, [{
        key: 'showQuickAddOptions',
        value: function showQuickAddOptions() {
            if (this.props.quickAddProduct) {
                this.setState(function (prevState) {
                    return {
                        quickAddOptionsVisible: !prevState.quickAddOptionsVisible
                    }
                })
            }
        }
    }, {
        key: 'restoreInitialState',
        value: function restoreInitialState() {
            var _props = this.props,
                hasHoverImage = _props.hasHoverImage,
                quickAddProduct = _props.quickAddProduct;
            if (hasHoverImage) {
                this.setState({
                    activeImage: this.state.initialImage
                })
            }
            if (quickAddProduct) {
                this.setState({
                    quickAddOptionsVisible: false,
                    variantImageUrl: null
                })
            }
        }
    }, {
        key: 'changeImage',
        value: function changeImage(variants) {
            if (!variants) {
                return null
            }
            var variantImageUrl = variants.model[0].getImageBySize('huge_url');
            this.setState({
                variantImageUrl: variantImageUrl
            })
        }
    }, {
        key: 'showHoverImage',
        value: function showHoverImage() {
            var hasHoverImage = this.props.hasHoverImage;
            var hoverImage = this.state.hoverImage;
            if (!hasHoverImage || !hoverImage) {
                return null
            }
            this.setState({
                activeImage: hoverImage
            })
        }
    }, {
        key: 'showColorImage',
        value: function showColorImage(color) {
            var product = this.props.product;
            var colorVariants = product.getVariants().model.filter(function (variant) {
                return _lodash2.default.find(variant.getOptionValues(), function (option_value) {
                    return option_value.option_type_name === 'color' && option_value.name === color
                })
            });
            if (colorVariants.length) {
                var variantImageUrl = colorVariants[0].getImageBySize('huge_url');
                this.setState({
                    variantImageUrl: variantImageUrl
                })
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;
            var _props2 = this.props,
                product = _props2.product,
                page = _props2.page,
                hasWishlist = _props2.hasWishlist,
                hasSizes = _props2.hasSizes,
                hasColors = _props2.hasColors,
                imageSize = _props2.imageSize,
                imageCdnOptions = _props2.imageCdnOptions,
                quickAddProduct = _props2.quickAddProduct,
                nameAsLink = _props2.nameAsLink,
                showPropertyAsDescription = _props2.showPropertyAsDescription,
                propertyName = _props2.propertyName;
            var _state = this.state,
                activeImage = _state.activeImage,
                variantImageUrl = _state.variantImageUrl;
            var salePrice = product.getMasterVariant().getDisplaySalePrice();
            var saleName = product.getMasterVariant().getSaleName();
            var classesPrice = (0, _classnames2.default)('product__price', {
                'product__price--is-sale': salePrice
            });
            // var product_brand = '';
            // if (product.model && product.model.product_properties && product.model.product_properties.length > 0) {
            //     for (var index = 0; index < product.model.product_properties.length; index++) {
            //         if (product.model.product_properties[index].property_name == 'brand') {
            //             product_brand = product.model.product_properties[index].value;
            //             break;
            //         }
            //     }
            // }

            var product_brand = [];
            if (product.model && product.model.main_taxons && product.model.main_taxons.length > 0) {
                for (var index = 0; index < product.model.main_taxons.length; index++) {
                    product_brand.push(product.model.main_taxons[index].name);                    
                }
            }
            product_brand =product_brand.join(', ');

            return _react2.default.createElement('div', {
                className: 'product__container',
                itemScope: true,
                itemType: 'http://schema.org/Product'
            }, salePrice && saleName && _react2.default.createElement('span', {
                className: 'product__sale-name'
            }, saleName), _react2.default.createElement('div', {
                className: 'product__img-container',
                onMouseLeave: function onMouseLeave() {
                    return _this2.restoreInitialState()
                },
                onMouseOver: function onMouseOver() {
                    return _this2.showHoverImage()
                }
            }, _react2.default.createElement(_reactRouterDom.Link, {
                to: '/product/' + product.getProperty('slug')
            }, _react2.default.createElement(_Image2.default, {
                itemProp: 'image',
                src: variantImageUrl || activeImage,
                alt: product.getName()
            })), quickAddProduct && _react2.default.createElement('div', {
                className: 'product__quick-add-container'
            }, _react2.default.createElement('button', {
                className: 'product__quick-add-button',
                onClick: function onClick() {
                    return _this2.showQuickAddOptions()
                }
            }, page && page.getTemplateValueByField('quick_add_button_text')), this.state.quickAddOptionsVisible && _react2.default.createElement(_QuickAddProduct2.default, {
                changeImage: this.changeImage.bind(this),
                product: product
            }))), renderWishlistHeart(this.props), _react2.default.createElement('div', {
                className: 'product__info'
            }, _react2.default.createElement('h3', {
                className: 'p-brand-name',
            }, product_brand), nameAsLink ? _react2.default.createElement('h3', {
                className: 'product__name'
            }, _react2.default.createElement(_reactRouterDom.Link, {
                to: '/product/' + product.getProperty('slug'),
                itemProp: 'name'
            }, product.getName())) : _react2.default.createElement('h3', {
                className: 'product__name',
                itemProp: 'name'
            }, product.getName()), _react2.default.createElement('div', {
                className: 'product__description',
                itemProp: 'description',
                dangerouslySetInnerHTML: {
                    __html: product.getDescription()
                }
            }), showPropertyAsDescription && product.getProductProperty(propertyName) && _react2.default.createElement('div', {
                className: 'product__property-description',
                itemProp: 'description',
                dangerouslySetInnerHTML: {
                    __html: product.getProductProperty(propertyName)
                }
            }), _react2.default.createElement('span', {
                className: classesPrice,
                itemProp: 'price'
            }, product.getMasterVariant().getDisplayPrice()), salePrice && _react2.default.createElement('span', {
                className: 'product__price-sale'
            }, salePrice), hasSizes && renderSizes(product), hasColors && this.renderColors(product), _react2.default.createElement('div', {
                className: 'product__btn-container'
            }, _react2.default.createElement(_Button2.default, {
                href: '/product/' + product.getProperty('slug'),
                className: 'button button--block'
            }, page && page.getTemplateValueByField('product_item_button_text')))))
        }
    }]);
    return ProductItem
}(_react2.default.Component);
var _initialiseProps = function _initialiseProps() {
    var _this3 = this;
    this.renderColors = function (product) {
        var maxColorItems = 10;
        var colorsCollection = product.getVariants().getOptionValueByOptionTypeName('color');
        var extraColorsCount = colorsCollection.length - maxColorItems;
        var colors = (colorsCollection.slice(0, maxColorItems) || []).map(function (color) {
            var classes = (0, _classnames2.default)('product__color', {
                'product__color--out-of-stock': !color.getProperty('in_stock') && !color.getProperty('is_backorderable')
            });
            var styles = {
                'backgroundColor': '' + color.getHexColor()
            };
            return _react2.default.createElement('span', {
                className: classes,
                style: styles,
                key: color.getId(),
                onClick: function onClick() {
                    _this3.showColorImage(color.getName())
                }
            })
        });
        if (extraColorsCount > 0) {
            return _react2.default.createElement('div', {
                className: 'product__colors'
            }, colors, ' +', extraColorsCount)
        }
        return _react2.default.createElement('div', {
            className: 'product__colors'
        }, colors)
    }
};;
ProductItem.defaultProps = {
    hasWishlist: _cqBaseReact.cqApi.cqApi.getConfiguration().options && _cqBaseReact.cqApi.cqApi.getConfiguration().options.hasWishlist,
    imageSize: 'huge_url',
    imageCdnOptions: undefined,
    quickAddProduct: false,
    nameAsLink: false,
    hasHoverImage: false,
    propertyName: 'pop_description'
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
        openModal: function openModal(component) {
            dispatch(_cqBaseReact.actions.openModal(component))
        }
    }
};
exports.default = (0, _withOverride2.default)((0, _Translations2.default)((0, _reactRedux.connect)(null, mapDispatchToProps)((0, _withSettings2.default)(ProductItem, 'ProductItem')), ['common']), 'ProductItem');
module.exports = exports['default'];