'use strict';
Object.defineProperty(exports, '__esModule', {
    value: true
});
exports.Menu = undefined;
var _react = require('react');
var _lodash = require('lodash');
var _react2 = _interopRequireDefault(_react);
var _classnames = require('classnames');
var _classnames2 = _interopRequireDefault(_classnames);
var _reactRouterDom = require('react-router-dom');
var _cqBaseReact = require('cq-base-react');
var _Image = require('../../UI/Image');
var _Image2 = _interopRequireDefault(_Image);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    }
}
var renderItem = function renderItem(menuItem, onLinkClick, showIcon, isSubItem) {
    var id = menuItem.id,
        label = menuItem.label,
        link_to = menuItem.link_to,
        menu_items = menuItem.menu_items,
        type = menuItem.type,
        icon = menuItem.icon,
        iconHover = menuItem.iconHover,
        main_image = menuItem.main_image;
    var itemClass = (0, _classnames2.default)('menu__item', {
        'menu__item--has-sub': menu_items.length
    }, {
            'menu__item--has-image': main_image
        });
    if (main_image) {
        return _react2.default.createElement('li', {
            className: itemClass,
            key: id
        }, _react2.default.createElement(_reactRouterDom.NavLink, {
            className: 'menu__item-link-image',
            to: _cqBaseReact.libs.getHref(link_to, type) || '/',
            onClick: onLinkClick
        }, _react2.default.createElement('img', {
            src: _cqBaseReact.libs.getImageByUrl(main_image),
            alt: label,
            className: 'menu__item-image'
        })), _react2.default.createElement(_reactRouterDom.NavLink, {
            className: 'menu__item-link',
            to: _cqBaseReact.libs.getHref(link_to, type) || '/',
            onClick: onLinkClick
        }, label))
    }
    var isExternalLink = link_to.includes('http');
    if (isExternalLink) {
        return _react2.default.createElement('li', {
            className: itemClass,
            key: id
        }, _react2.default.createElement('a', {
            href: link_to,
            className: 'menu__item-link',
            target: '_blank',
            onClick: onLinkClick
        }, label))
    }
    return _react2.default.createElement('li', {
        className: itemClass,
        key: id
    }, _react2.default.createElement(_reactRouterDom.NavLink, {
        activeClassName: 'menu__item-link--active',
        className: 'menu__item-link',
        to: _cqBaseReact.libs.getHref(link_to, type) || '/',
        onClick: onLinkClick,
        isActive: function isActive(match, location) {
            if (!match) {
                return location.pathname + location.search === _cqBaseReact.libs.getHref(link_to, type)
            }
            return match && match.isExact
        }
    }, showIcon && icon && _react2.default.createElement('img', {
        className: 'menu__item-icon',
        src: _cqBaseReact.libs.getImageByUrl(icon),
        alt: ''
    }), showIcon && iconHover && _react2.default.createElement('img', {
        className: 'menu__item-icon-hover',
        src: _cqBaseReact.libs.getImageByUrl(iconHover),
        alt: ''
    }), label), renderMenu({
        menuItems: menu_items,
        className: 'menu__items--sub',
        onLinkClick: onLinkClick,
        parentImage: menuItem.menu_image
    }))
};
var renderItems = function renderItems(menuItems, onLinkClick, showIcon, isSubItem) {
    return (menuItems || []).map(function (menuItem, index) {
        isSubItem = false;
        if (menuItem.parent_id) {
            isSubItem = true;
        }
        return renderItem(menuItem, onLinkClick, showIcon, !isSubItem)
    })
};

var distrbutedItems = function distrbutedItems(subCats, classesName, onLinkClick, showIcon) {
    return (subCats || []).map(function (item, index) {
        return _react2.default.createElement('ul', {
            className: classesName
        }, _react2.default.createElement('li', {
            className: 'menu__item title',
        }, item.key), renderItems(item.values, onLinkClick, showIcon, false));
    });
};

var renderMenu = function renderMenu(props) {
    var isSubMenu = false;
    if (props.menuItems && props.menuItems.length > 0 && props.menuItems[0].parent_id) {
        isSubMenu = true;
    }
    var menuItems = props.menuItems,
        className = props.className,
        onLinkClick = props.onLinkClick,
        showIcon = props.showIcon;
    if (menuItems && menuItems.length) {
        var classesName = (0, _classnames2.default)('menu__items', className);
        var subListcount = '0';
        var groupByCategories = {};
        var subCats = [];
        if (props.menuItems[0].menu_type) {
            subListcount = '2';
            groupByCategories = _lodash.groupBy(props.menuItems, 'menu_type');
            var empty = [];
            Object.keys(groupByCategories).forEach(function (key) {
                if (!key) {
                    empty.push(groupByCategories[key]);
                } else {
                    var tempObj = {
                        key: key,
                        values: groupByCategories[key]
                    };
                    subCats.push(tempObj);
                }
            });
        }
        if (isSubMenu) {
            return _react2.default.createElement('div', {
                className: "cat-sub-main",
                key: Math.floor(Math.random() * (1000 - 1 + 1)) + 1,
            }, subCats.length > 0 && _react2.default.createElement('div', {
                className: (props.parentImage ? '' : 'no-image ') + "cat-sub-list " + 'count-' + subListcount,
                key: Math.floor(Math.random() * (1000 - 1 + 1)) + 1,
            }, distrbutedItems(subCats, classesName, onLinkClick, showIcon)),
                subCats.length < 1 && _react2.default.createElement('div', {
                    className: (props.parentImage ? '' : 'no-image ') + "cat-sub-list " + 'count-' + subListcount,
                    key: Math.floor(Math.random() * (1000 - 1 + 1)) + 1,
                }, _react2.default.createElement('ul', {
                    className: classesName
                }, renderItems(menuItems, onLinkClick, showIcon, false))),
                props.parentImage && _react2.default.createElement('div', {
                    className: "cat-sub-image",
                    key: Math.floor(Math.random() * (1000 - 1 + 1)) + 1,
                }, _react2.default.createElement(_Image2.default, {
                    itemProp: 'image',
                    src: _cqBaseReact.libs.getImageByUrl(props.parentImage),
                    alt: "Category"
                })));
        }
        else {
            var classesName = (0, _classnames2.default)('menu__items parent', className);
            return _react2.default.createElement('ul', {
                className: classesName
            }, renderItems(menuItems, onLinkClick, showIcon, false))
        }
        // return _react2.default.createElement('ul', {
        //     className: classesName
        // }, renderItems(menuItems, onLinkClick, showIcon, false))
    }
    return null
};
var Menu = exports.Menu = function Menu(props) {
    return renderMenu(props)
};